let student = [

  { StudentName : "Student 1",StudentID : "S1", Subject1 : 100, Subject2: 98, Subject3: 76, Subject4: 82, Subject5: 92 },    
  { StudentName: "Student 2", StudentID: "S2", Subject1: 92, Subject2: 100, Subject3: 98, Subject4: 76, Subject5: 82 },
  { StudentName : "Student 3", StudentID : "S3", Subject1 : 76, Subject2: 92, Subject3: 100, Subject4: 98, Subject5: 76 },
  { StudentName : "Student 4", StudentID : "S4", Subject1 : 82, Subject2: 76, Subject3: 92, Subject4: 100, Subject5: 98 }
]
// Require the framework and instantiate it
const fastify = require('fastify')({ logger: false })

// Declare a route
fastify.get('/', async (request, reply) => {
  return { hello: 'Exeter', message: 'Programming Challenge 2' }
})

fastify.get('/report', async(request, reply) => {
  reply
      .code(200)
      .header('Content-Type', 'application/json; charset=utf-8')
      .send(student)
})

fastify.post('/add', async(request, reply) => {
  student.push(request.body)
  reply
      .code(201)
      .header('Content-Type', 'application/json; charset=utf-8')
      .send(student[student.length - 1])
});

fastify.post('/update', async(request, reply) => {
  let req = request.body
  let dataKey = Object.keys(request.body)
  i = student.findIndex(obj => obj["StudentID"] == req["StudentID"])
  student[i][dataKey[1]] = req[dataKey[1]]
  reply
      .code(200)
      .header('Content-Type', 'application/json; charset=utf-8')
      .send(student)
});

fastify.delete('/delete', async(request, reply) => {
  let id = request.body
  d = student.findIndex(obj => obj["StudentID"] == id["StudentID"])
  student.splice(d,1)
  reply
      .code(200)
      .header('Content-Type', 'application/json; charset=utf-8')
      .send(student)
});
// Run the server!
const start = async () => {
  try {
    await fastify.listen(3000)
    console.log("Server Listening @3000");
  } catch (err) {
    fastify.log.error(err)
    process.exit(1)
  }
}
start()